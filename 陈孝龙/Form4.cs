﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private int id;
        private string studentName;
        private int age;
        private int score;
        public Form4(int id,string name,int age,int score)
        {
            this.id = id;
            this.studentName = name;
            this.age = age;
            this.score = score;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
