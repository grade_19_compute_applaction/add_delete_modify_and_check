﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlInterface
{
    static class DBHelper
    {
        //连接
        private static SqlConnection GetConnection()
        {
            string connStr = "server = . ; database = dbdemo ; uid = sa ; pwd = 193746";
            SqlConnection conn = new SqlConnection(connStr);
            return conn;
        }
        //获取数据
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            conn.Close();
            return dt;
        }

        //编辑数据(返回影响行数)
        public static int EditData(string sql)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlCommand cmd = new SqlCommand(sql, conn);
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            return i;
        }
    }
}
