﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var abc = "select * from users";

            var stuSql = "select* from students";

            var dt = DbHelper.GetDataTable(abc);

            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        //删除
       
        private void button5_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where StudentName like '%{0}%'", name);
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        //添加
        private void button6_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            form.Show();
        }


        //更新
        private void button7_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            form.Show();

            var row = dataGridView1.SelectedRows[0];
            var id = (int)row.Cells["Id"].Value;
            var name = (string)row.Cells["StuName"].Value;
            var age = (int)row.Cells["Age"].Value;
            var score = (int)row.Cells["Score"].Value;
            Form3 form = new Form3(id, name, age, score);
            var res = Form2.ShowDialog();

            if (res == DialogResult.Yes)
            {

                var stuSql = "select * from student";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        //删除
        private void button8_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            var id = (int)row.Cells["Id"].Value;

            var sql = string.Format("delete from student where Id={0}", id);

            var res = DbHelper.AddOrUpdateOrDelete(sql);
            if (res == 1)
            {
                MessageBox.Show("删除成功！", "提示");
                var stuSql = "select * from student";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除成功！", "提示");
            }
        }
    }
}
