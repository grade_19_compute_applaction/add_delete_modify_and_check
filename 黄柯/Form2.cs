﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private int id;

        public Form2(int id, string sName, int age, int score)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = sName;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();
        }

        //取消
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        //确定保存
        private void button1_Click(object sender, EventArgs e)
        {

            // SqlCommand command = new SqlCommand();
            // command.ExecuteNonQuery();

            var sName = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;


            if (this.id > 0)
            {
                //更新
                var sql = string.Format("update students set sName='{0}',Age={1},Score={2} where Id={3}", sName, age, score, this.id);

                Dbhelper.AddOrUpdate(sql);

                MessageBox.Show("更新成功！", "提示");

            }

            //添加
            else
            {

                var sql = string.Format("insert into students(sName,Age,Score) values ('{0}',{1},{2})", sName, age, score);

                Dbhelper.AddOrUpdate(sql);

                MessageBox.Show("添加成功！", "提示");


            }

            this.DialogResult = DialogResult.Yes;

            this.Close();

        }



        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }



    }
}
