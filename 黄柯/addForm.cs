﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class addForm : Form
    {     

        public addForm()
        {
            InitializeComponent();
            
        }

        private int id;
        public addForm(int id, string sName, int age, int score)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = sName;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();


        }

        private void addForm_Load(object sender, EventArgs e)
        {
            //SqlCommand command = new SqlCommand();

            //command.ExecuteNonQuery();
        }

        //取消
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        //确定保存
        private void button1_Click(object sender, EventArgs e)
        {
            //SqlCommand command = new SqlCommand();

            var sName = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;

            if (this.id > 0)
            {
                var sql = string.Format("update students set sname='{0}',age={1},score={2} where id={3}", sName, age, score, id);

                ConnectData.GetDataTable(sql);

                MessageBox.Show("更新成功", "提示");
            }
            //添加
            else
            {
                var sql = string.Format(" insert into students (sName,age,score) values('{0}',{1},{2}", sName, age, score);
                ConnectData.AddOrUpadte(sql);
                MessageBox.Show("添加成功", "提示");
        }

            this.DialogResult = DialogResult.Yes;
            this.Close();
            //return DialogResult.Yes;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
