create database school
go

use school

go

create table student
( 
	Id int primary key identity(1,1) not null,
	StuName varchar(10) not null,
	Age int default 18,
	Score int default 0,
)
insert into student (StuName,Score) values('张三',87),
										  ('小北',68),
										  ('林北',79),
										  ('林书航',77),
										  ('张焕',66)	
delete from student