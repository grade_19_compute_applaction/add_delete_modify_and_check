﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DBHelper
    {
        private static string conString = "server =.;database=test;uid=sa;pwd=123456;";

        public static DataTable GetDataTable(string sql)
        {

            SqlConnection conn = new SqlConnection(conString);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);

            conn.Open();

            DataTable dt = new DataTable();

            adapter.Fill(dt);

            return dt;
        }

        public static int AddOrUpdateOrDelect(string sql)
        {
            SqlConnection conn = new SqlConnection(conString);

            SqlCommand com = new SqlCommand(sql, conn);

            conn.Open();

            return com.ExecuteNonQuery();
        }
    }
}
