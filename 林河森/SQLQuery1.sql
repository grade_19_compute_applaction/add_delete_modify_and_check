USE master
GO
IF EXISTS(SELECT * FROM SysDataBases WHERE NAME = 'Schools')
DROP DATABASE Schools
GO
CREATE DATABASE Schools
GO
USE Schools
GO
IF EXISTS(SELECT * FROM SysObjects WHERE NAME = 'Students')
DROP TABLE Students
GO
CREATE TABLE Students
(
	StudentsId INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	StudentsName NVARCHAR(80) UNIQUE NOT NULL,
	StudentsAge INT DEFAULT 18, 
	StudentsScore INT DEFAULT 0,
)
GO

INSERT INTO Students (StudentsName) VALUES ('演员')

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('张三',18,88)

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('李四',20,90)

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('王五',21,92)

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('林六',19,95)

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('张七',17,84)

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('李八',22,96)

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('王九',23,78)

INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('林十',20,100)
GO

/*SELECT * FROM Students

SELECT * FROM Students WHERE StudentsName LIKE '%林%'

SELECT * FROM Students

UPDATE Students set StudentsName='',StudentsAge= ,StudentsScore= where StudentsId=*/