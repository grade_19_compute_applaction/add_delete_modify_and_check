﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
        }

        private int id;

        public AddForm(int id,string name,int age,int score)
        {
            InitializeComponent();

            this.id = id;
            AddNameBox.Text = name;
            AddAgeBox.Text = age.ToString();
            AddScoreBox.Text = score.ToString();

        }

        private void CancelPreserve_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void DefinitePreserve_Click(object sender, EventArgs e)
        {
            var name = AddNameBox.Text;
            var age = AddAgeBox.Text;
            var score = AddScoreBox.Text;


            //修改
            if(this.id>0)
            {
                var sql = string.Format("UPDATE Students set StudentsName='{0}',StudentsAge={1},StudentsScore={2} where StudentsId={3}", name, age, score, this.id);

                DbHelper.AddOrChangeOrDelete(sql);

                MessageBox.Show("修改成功!", "提示");

            }
            //增加
            else
            {
                var sql = string.Format("INSERT INTO Students (StudentsName,StudentsAge,StudentsScore) VALUES ('{0}',{1},{2})", name, age, score);

                DbHelper.AddOrChangeOrDelete(sql);

                MessageBox.Show("增加成功", "提示");

            }

            this.DialogResult = DialogResult.Yes;

            this.Close();

        }
    }
}
