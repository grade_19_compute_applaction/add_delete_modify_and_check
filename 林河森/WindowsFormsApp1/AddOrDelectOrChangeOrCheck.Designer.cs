﻿namespace WindowsFormsApp1
{
    partial class AddOrDelectOrChangeOrCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.schoolsDataSet = new WindowsFormsApp1.SchoolsDataSet();
            this.studentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.studentsTableAdapter = new WindowsFormsApp1.SchoolsDataSetTableAdapters.StudentsTableAdapter();
            this.ADCCName = new System.Windows.Forms.Label();
            this.StuNameBox = new System.Windows.Forms.TextBox();
            this.StuAdd = new System.Windows.Forms.Button();
            this.StuDelect = new System.Windows.Forms.Button();
            this.StuChange = new System.Windows.Forms.Button();
            this.StuCheck = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(-9, 171);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(700, 200);
            this.dataGridView1.TabIndex = 0;
            // 
            // schoolsDataSet
            // 
            this.schoolsDataSet.DataSetName = "SchoolsDataSet";
            this.schoolsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // studentsBindingSource
            // 
            this.studentsBindingSource.DataMember = "Students";
            this.studentsBindingSource.DataSource = this.schoolsDataSet;
            // 
            // studentsTableAdapter
            // 
            this.studentsTableAdapter.ClearBeforeFill = true;
            // 
            // ADCCName
            // 
            this.ADCCName.AutoSize = true;
            this.ADCCName.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ADCCName.Location = new System.Drawing.Point(49, 69);
            this.ADCCName.Name = "ADCCName";
            this.ADCCName.Size = new System.Drawing.Size(89, 29);
            this.ADCCName.TabIndex = 1;
            this.ADCCName.Text = "姓名:";
            // 
            // StuNameBox
            // 
            this.StuNameBox.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StuNameBox.Location = new System.Drawing.Point(127, 70);
            this.StuNameBox.Name = "StuNameBox";
            this.StuNameBox.Size = new System.Drawing.Size(141, 31);
            this.StuNameBox.TabIndex = 2;
            // 
            // StuAdd
            // 
            this.StuAdd.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StuAdd.Location = new System.Drawing.Point(378, 69);
            this.StuAdd.Name = "StuAdd";
            this.StuAdd.Size = new System.Drawing.Size(89, 31);
            this.StuAdd.TabIndex = 3;
            this.StuAdd.Text = "增加";
            this.StuAdd.UseVisualStyleBackColor = true;
            this.StuAdd.Click += new System.EventHandler(this.StuAdd_Click);
            // 
            // StuDelect
            // 
            this.StuDelect.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StuDelect.Location = new System.Drawing.Point(568, 70);
            this.StuDelect.Name = "StuDelect";
            this.StuDelect.Size = new System.Drawing.Size(89, 31);
            this.StuDelect.TabIndex = 3;
            this.StuDelect.Text = "删除";
            this.StuDelect.UseVisualStyleBackColor = true;
            this.StuDelect.Click += new System.EventHandler(this.StuDelect_Click);
            // 
            // StuChange
            // 
            this.StuChange.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StuChange.Location = new System.Drawing.Point(473, 70);
            this.StuChange.Name = "StuChange";
            this.StuChange.Size = new System.Drawing.Size(89, 31);
            this.StuChange.TabIndex = 3;
            this.StuChange.Text = "修改";
            this.StuChange.UseVisualStyleBackColor = true;
            this.StuChange.Click += new System.EventHandler(this.StuChange_Click);
            // 
            // StuCheck
            // 
            this.StuCheck.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StuCheck.Location = new System.Drawing.Point(283, 69);
            this.StuCheck.Name = "StuCheck";
            this.StuCheck.Size = new System.Drawing.Size(89, 31);
            this.StuCheck.TabIndex = 3;
            this.StuCheck.Text = "查询";
            this.StuCheck.UseVisualStyleBackColor = true;
            this.StuCheck.Click += new System.EventHandler(this.StuCheck_Click);
            // 
            // AddOrDelectOrChangeOrCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.StuCheck);
            this.Controls.Add(this.StuChange);
            this.Controls.Add(this.StuDelect);
            this.Controls.Add(this.StuAdd);
            this.Controls.Add(this.StuNameBox);
            this.Controls.Add(this.ADCCName);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AddOrDelectOrChangeOrCheck";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddOrDelectOrChangeOrCheck";
            this.Load += new System.EventHandler(this.AddOrDelectOrChangeOrCheck_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private SchoolsDataSet schoolsDataSet;
        private System.Windows.Forms.BindingSource studentsBindingSource;
        private SchoolsDataSetTableAdapters.StudentsTableAdapter studentsTableAdapter;
        private System.Windows.Forms.Label ADCCName;
        private System.Windows.Forms.TextBox StuNameBox;
        private System.Windows.Forms.Button StuAdd;
        private System.Windows.Forms.Button StuDelect;
        private System.Windows.Forms.Button StuChange;
        private System.Windows.Forms.Button StuCheck;
    }
}