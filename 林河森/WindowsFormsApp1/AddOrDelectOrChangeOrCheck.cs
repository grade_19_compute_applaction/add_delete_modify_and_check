﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AddOrDelectOrChangeOrCheck : Form
    {
        public AddOrDelectOrChangeOrCheck()
        {
            InitializeComponent();
        }

        private void AddOrDelectOrChangeOrCheck_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet.Students”中。您可以根据需要移动或删除它。

            var StuSql = "SELECT * FROM Students";

            var dt = DbHelper.GetDataTable(StuSql);

            dataGridView1.DataSource = dt;

            //限制DataGridView的某些行为
            //选择整行
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //不能编辑
            dataGridView1.ReadOnly = true;
            //不显新行
            dataGridView1.AllowUserToAddRows = false;
        }
        
        //增加
        private void StuAdd_Click(object sender, EventArgs e)
        {
            AddForm form = new AddForm();

            var res=form.ShowDialog();

            if(res==DialogResult.Yes)
            {
                var StuSql = "SELECT * FROM Students";

                var dt = DbHelper.GetDataTable(StuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        //删除
        private void StuDelect_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["StudentsId"].Value;

                var sql = string.Format("DELETE FROM Students WHERE StudentsId={0}", id);

                var res = DbHelper.AddOrChangeOrDelete(sql);

                if (res == 1)
                {
                    MessageBox.Show("删除成功!", "提示");

                    var StuSql = "SELECT * FROM Students";

                    var dt = DbHelper.GetDataTable(StuSql);

                    dataGridView1.DataSource = dt;

                }
                else
                {
                    MessageBox.Show("删除失败!", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据", "提示");
            }

            
        }

        //修改
        private void StuChange_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["StudentsId"].Value;

            var name = (string)dataGridView1.SelectedRows[0].Cells["StudentsName"].Value;

            var age = (int)dataGridView1.SelectedRows[0].Cells["StudentsAge"].Value;

            var score = (int)dataGridView1.SelectedRows[0].Cells["StudentsScore"].Value;

            AddForm form = new AddForm(id,name,age,score);

            var res = form.ShowDialog();

            if (res==DialogResult.Yes)
            {
                var StuSql = "SELECT * FROM Students";

                var dt = DbHelper.GetDataTable(StuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        //查询
        private void StuCheck_Click(object sender, EventArgs e)
        {
            var name = StuNameBox.Text;

            var sql =string.Format("SELECT * FROM Students WHERE StudentsName LIKE '%{0}%'",name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;
        }
    }
}
