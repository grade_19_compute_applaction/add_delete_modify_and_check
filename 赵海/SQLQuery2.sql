create table Users
(
	Id int primary key identity,
	Name nvarchar(80)not null,
	Age int default 18,
	Score int default 0,
)
go

insert into Users (Name) values ('小陈')
insert into Users (Name) values ('旺财')
insert into Users (Name) values ('小白')
insert into Users (Name) values ('黑狗')
insert into Users (Name) values ('香花')
insert into Users (Name) values ('翠花')
insert into Users (Name) values ('芦花')

select * from Users where Name like '%花%'