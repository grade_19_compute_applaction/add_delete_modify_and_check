﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“usersDataSet.Usersinfo”中。您可以根据需要移动或删除它。
            this.usersinfoTableAdapter.Fill(this.usersDataSet.Usersinfo);
        
            var sql = "select * from Usersinfo";
            var dataTable = DBHelper.GetDataTable(sql);

            //限制
            dataGridView1.DataSource = dataTable; //获得数据库信息
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }
        //查找
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Usersinfo where name like '%{0}%'", name);

            var dataTable = DBHelper.GetDataTable(sql);
            dataGridView1.DataSource = dataTable;
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["name"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["age"].Value;
            var sex = (string)dataGridView1.SelectedRows[0].Cells["sex"].Value;

            Form2 form2 = new Form2(id, name, age, sex);
            form2.Show();
        }
        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];
            var cell = row.Cells;

            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var sql = string.Format("delete from Usersinfo where id ={0}", id);
            var a = DBHelper.Add(sql);

            if (a == 1)
            {
                MessageBox.Show("删除成功！", "提示");

                var sql1 = "select * from Usersinfo";
                var dataTable = DBHelper.GetDataTable(sql1);
                dataGridView1.DataSource = dataTable;
            }
            else
            {
                MessageBox.Show("删除失败，请重试！","提示");
            }
        }
    }
}
