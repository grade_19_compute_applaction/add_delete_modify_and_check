﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTestApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“adminDataSet.Users”中。您可以根据需要移动或删除它。

            var com = "select * from commodity";

            var dt = DbHelper.GetDataTable(com);

            dataGridView1.DataSource = dt;

            //限制DataGridView的某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from commodity where commodityName like '%{0}%'",name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res = form.ShowDialog();
            if (res==DialogResult.Yes)
            {
                var com = "select * from commodity";

                var dt = DbHelper.GetDataTable(com);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["commodityId"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["commodityName"].Value;
            var price = (int)dataGridView1.SelectedRows[0].Cells["price"].Value;
            var inventory = (int)dataGridView1.SelectedRows[0].Cells["inventory"].Value;
            EditForm form = new EditForm(id,name,price,inventory);
            var res = form.ShowDialog();
            if (res==DialogResult.Yes)
            {
                var com = "select * from commodity";

                var dt = DbHelper.GetDataTable(com);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }

        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["commodityId"].Value;
                var sql = string.Format("delete from commodity where commodityId={0}", id);
                var res = DbHelper.AddOrUpdate(sql);

                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");
                    var com = "select * from commodity";
                    var dt = DbHelper.GetDataTable(com);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据", "提示");
            }
        }
    }
}
