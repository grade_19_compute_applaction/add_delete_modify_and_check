﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTestApp1
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }

        private int id;

        public EditForm(int id,string name,int price,int inventory)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = price.ToString();
            textBox3.Text = inventory.ToString();
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var price = textBox2.Text;
            var inventory = textBox3.Text;

            //更新
            if (this.id>0)
            {
                var sql = string.Format("update commodity set commodityName = '{0}',price = {1},inventory = {2} where commodityId = {3}", name, price, inventory, this.id);

                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("更新成功","提示");
            }
            else
            {
                var sql = string.Format("insert into commodity(commodityName,price,inventory) values('{0}','{1}','{2}')", name, price, inventory);

                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("添加成功", "提示");
            }

            this.DialogResult = DialogResult.Yes;

            this.Close();
        }
    }
}
