create table Users
(
	ID int primary key identity(1,1) ,
	Name varchar(80) not null,	
	Age int not null,   
	Sex char(2) not null
)
go

insert into Users(Name,Age,Sex) values ('李四',10,'男')
insert into Users(Name,Age,Sex) values ('张三',18,'男')
insert into Users(Name,Age,Sex) values ('小红',10,'女')
insert into Users(Name,Age,Sex) values ('想想',10,'女')
go
select * from Users

CREATE TABLE commodity
(
	commodityId INT IDENTITY(1,1) PRIMARY KEY,
	commodityName VARCHAR(80) UNIQUE NOT NULL,
	price INT,
	inventory INT
)
go

INSERT INTO commodity
VALUES
	('惠普暗影精灵5' , 6999 , null) , ('惠普战66 三代' , 5299 , 317 ),
	('小米10' , 3999 , 309 ) , ('小米Mix3' , 2649 , 372 ),
	('云南白药牙膏' , 27 , 528 ) , (' 黑人超白密泡小苏打牙膏' , 2999 , 598 ),
	('联想小新Pro13' , 7299 , 86 ),
	('联想拯救者Y7000P' , 8299 , 132 ),
	('华硕飞行堡垒7' , 6799 , 46 ),
	('华硕ARTONE' , 13999 , 21 ),
	('HUAWEI Mate 30' , 4499 , 57 ),
	('HUAWEI nova 6' , 3499 , 13 ),
	('Apple iPhone SE' , 3299 , null ),
	('Apple iPhone11 Pro' , 8699 , 56 ),
	('小麦秸秆漱口杯' , 8 , 231 ),
	('晾衣架子通用型' , 19 , 310),
	('特步男装休闲T恤' , 50 , 30 ),
	('麦克丹奴 工装裤男休闲裤' , 128 , 59 ),
	('惠普ENVY 13' , 6199 , 203 )
go	
select * from commodity
