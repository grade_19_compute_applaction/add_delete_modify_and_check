USE master
GO
IF EXISTS (SELECT * FROM SysDataBases WHERE NAME ='school')
DROP DATABASE school   ----删除数据库
GO
CREATE DATABASE school----创建数据库
ON PRIMARY---主要的，基本的
(
     NAME='school',                                     ---名称
     FILENAME='D:\关于SQL文件\school.mdf',      ---文件位置
     SIZE=5MB,                                             ---大小
     FILEGROWTH=1MB                               ---文件增长率
)
LOG ON        ---登录
(
    NAME='school_log',                                     ---名称
     FILENAME='D:\关于SQL文件\school_log.ldf',      ---文件位置
     SIZE=5MB,                                             ---大小
     FILEGROWTH=10%   
)
GO
USE school
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME='employee')
DROP TABLE employee
GO
CREATE TABLE employee ---创建员工表
(
    employeeid int identity(1,1 )primary key,---列名叫employeeid,数据类型为整形，约束是主键
	employeename varchar(20)not null,
	employeeage int not null,
	employeesex char(2) check(employeesex='男' or employeesex='女'),
	employeeeduca nvarchar(50),
	employeenative nvarchar(20),
	departid int not null,
	entrydata  int not null
	)
	---插入数据的格式

   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('张三',23,'男','高中','贵州省',1001,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('小王',26,'男','高中','湖北省',1002,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('张林',23,'男','高中','四川省',1003,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('黄灿',23,'男','高中','北京省',1004,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('李伟',23,'男','高中','贵州省',1005,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('夏馨',23,'男','高中','安徽省',1006,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('李涵月',23,'男','高中','湖南省',1007,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('李书',23,'男','高中','贵州省',1008,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('韩涵涵',24,'女','高中','四川省',1009,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('李四',25,'男','高中','山东省',1010,2020)
   insert into employee(employeename,employeeage,employeesex,employeeeduca,employeenative,departid,entrydata ) values('简语',26,'女','高中','江苏省',1011,2020)
 ---基本的查询的语句
   select * from employee

   select * from employee where employeename like '%涵%'

   