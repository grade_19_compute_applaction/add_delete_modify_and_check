﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WinformTest_Com_0002
{
    public class DbHelper
    {
        private static readonly string conString = "server=.;database=Schools;uid=sa;pwd=wzq0307";

        public static DataTable GetDataTable(string sql) 
        {
            SqlConnection connection = new SqlConnection(conString);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

            DataTable dt = new DataTable();

            connection.Open();

            adapter.Fill(dt);

            return dt;

        }
        public static int AddOrUpdateOrDelete(string sql) 
        {
            SqlConnection connection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql, connection);

            connection.Open();

            return command.ExecuteNonQuery();
        }
    }
}
