﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTest_Com_0002
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var stusql = "select * from Students";

            var data = DbHelper.GetDataTable(stusql);

            dataGridView1.DataSource = data;

            //限制DataGridView1的某些行为
            //控制整行
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            //禁止编辑
            dataGridView1.ReadOnly = true;

            //不显示新行
            dataGridView1.AllowUserToAddRows = false;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where Name like '%{0}%'", name);

            var data = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = data;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stusql = "select * from Students";

                var data = DbHelper.GetDataTable(stusql);

                dataGridView1.DataSource = data;
            }
            else
            {
                MessageBox.Show("确定取消？","提示");
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var name = (string)dataGridView1.SelectedRows[0].Cells["Name"].Value;
                var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
                var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;

                EditForm form = new EditForm(id, name, age, score);
                var res = form.ShowDialog();
                if (res == DialogResult.Yes)
                {
                    var stusql = "select * from Students";

                    var data = DbHelper.GetDataTable(stusql);

                    dataGridView1.DataSource = data;
                }
                else
                {
                    MessageBox.Show("确定取消保存？","提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要更新的数据", "提示");
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var sql = string.Format("delete from Students where Id={0}", id);
                var res = DbHelper.AddOrUpdateOrDelete(sql);

                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");
                    var stusql = "select * from Students";
                    var data = DbHelper.GetDataTable(stusql);
                    dataGridView1.DataSource = data;
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据", "提示");
            }

    }
    }
}
