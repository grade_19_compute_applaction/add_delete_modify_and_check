﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DbHelper
    {
      private static  readonly string  conString = "server = .;database=teacher;uid=sa;pwd=20010616;";
        public static DataTable GetDataTable(string sql) 
        { 
            SqlConnection connection = new SqlConnection(conString);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

            connection.Open();

            DataTable dt = new DataTable();

            adapter.Fill(dt);
            return dt;
        }

        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand(sql,connection);

            connection.Open();

            return command.ExecuteNonQuery();
        }
    }
}
