use master

GO

IF EXISTS(SELECT * FROM SysDatabAses WHERE Name ='MySchool')
DROP DATABASE MySchool

GO

CREATE DATABASE MySchool
ON PRIMARY
(
	NAME ='MySchool',
	FILENAME ='D:\MySchol.mdf',
	SIZE=5MB,
	FILEGROWTH=1MB

)
LOG ON
(
	NAME='MySchool_log',
	FILENAME='D:\MySchool_log.ldf',
	SIZE=1MB,
	FILEGROWTH=10%

)
GO
use MySchool
go
IF EXISTS(SELECT * FROM SysObjects where name ='student')
DROP TABLE student
CREATE TABLE student
(
	Id int primary key identity(1,1),
	Name varchar(20) not null,
	Age int not null,
	Score int not null

)
go

insert into student (Name,Age,Score) values('',20,'')
select * from student