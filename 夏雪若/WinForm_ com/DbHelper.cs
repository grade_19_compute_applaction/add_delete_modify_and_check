using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinForm__com
{
    public class DbHelper
    {
        private static readonly string conString = "server=.;database=SCHOOL;uid=sa;pwd=040520;";

        public static DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

            connection.Open();

            DataTable data = new DataTable();

            adapter.Fill(data);

            return data;
        }

        public static int AddOrUpdate(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql,connection);

            connection.Open();

            return command.ExecuteNonQuery();
        }
    }
}
