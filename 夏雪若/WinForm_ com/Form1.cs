using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForm__com
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“sCHOOLDataSet.stuinfo”中。您可以根据需要移动或删除它。
           
            //var clasql = "select * from class";
            var stusql = "select * from stuinfo";
           
            var data = DbHelper.GetDataTable(stusql);
            
            dataGridView1.DataSource = data;

            //限制DataGridView1的某些行为
            //控制整行
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            //禁止编辑
            dataGridView1.ReadOnly = true;

            //不显示新行
            dataGridView1.AllowUserToAddRows = false;

        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from stuinfo where stuName like '%{0}%'",name);

            var data = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = data;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stusql = "select * from stuinfo";
           
                var data = DbHelper.GetDataTable(stusql);
            
                dataGridView1.DataSource = data;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["stuId"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["stuName"].Value;
            var age=(int)dataGridView1.SelectedRows[0].Cells["stuAge"].Value;
            var score=(int)dataGridView1.SelectedRows[0].Cells["stuScore"].Value;

            EditForm form = new EditForm(id,name,age,score);
            var res=form.ShowDialog();
            if(res==DialogResult.Yes)
            {
                var stusql = "select * from stuinfo";
           
                var data = DbHelper.GetDataTable(stusql);
            
                dataGridView1.DataSource = data;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count>0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["stuId"].Value;
                var sql=string.Format("delete from stuinfo where stuId={0}",id);
                var res=DbHelper.AddOrUpdate(sql);

                if(res==1)
                {
                    MessageBox.Show("删除成功","提示");
                    var stusql = "select * from stuinfo";
                    var data = DbHelper.GetDataTable(stusql);
                    dataGridView1.DataSource = data;
                }
                else
                {  
                    MessageBox.Show("删除失败","提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据","提示");
            }
            
        }
    }
}
